# Path to your oh-my-zsh installation.
export ZSH=/Users/WimCrielaard/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="barmer"
DEFAULT_USER="WimCrielaard"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true" 
# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git docker mvn docker-machine git-taico workspace)

# User configuration

export PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/Library/TeX/texbin:/Library/Frameworks/Python.framework/Versions/2.7/bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

PAGER="vimpager"

weather() {
	curl "wttr.in/"$1
}
alias wttr="weather"

PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH
export CLASSPATH=".:/usr/local/Cellar/antlr/4.7/antlr-4.7-complete.jar:$CLASSPATH"
alias :q="exit"
alias :wq="exit"
alias :x="exit"
alias :q!="exit"
alias brexit="exit"
alias frexit="echo -e \"\e[31mfailed\";return 1"

alias emacs="nvim"
alias ed="nvim"
alias vi="nvim"
alias nano="nvim"
alias joe="nvim"
alias vim="nvim"

alias top="htop"

# sl and gti are both installed
alias sl="ls"
alias gti="git"

alias mn="man"

alias updatedb="sudo /usr/libexec/locate.updatedb"
eval $(thefuck --alias)
alias clc="clear"

alias jsc="/System/Library/Frameworks/JavaScriptCore.framework/Resources/jsc"

alias docer=docker
alias py=python3

#Fix tmux issue
alias tmux="tmux -u"

docker_killall() { docker rm $(docker ps -aq); }
docker_removeall() { docker rmi $(docker images -aq); }
docker_clean() { rm ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/Docker.qcow2; }

docker_destruct() {
	docker_killall
	docker_removeall
	docker_clean
}

# FSL Setup
FSLDIR=/usr/local/fsl
PATH=${FSLDIR}/bin:${PATH}
export FSLDIR PATH
. ${FSLDIR}/etc/fslconf/fsl.sh

update_all_zsh() {
	brew update
	brew upgrade
	brew cleanup
	tldr --update
	upgrade_oh_my_zsh
}

activate() {
	if [ "$1" = "hacker" ] && [ "$2" = "mode" ]; then
			cmatrix -s
			true
	elif [ "$1" = "eelco" ] && [ "$2" = "mode" ]; then
		if [ "$3" = "--hard" ]; then
			echo -e "\033]50;SetProfile=Eelco\a"
			clear
		else
			eelco
		fi
		true
	elif [ "$1" = "normal" ]; then
		echo -e "\033]50;SetProfile=Default\a"
		true
	else
		false
	fi
}

eelco() {
	if [ "$1" = "--hard" ] || [ "$1" = "--force" ]; then
		clear
		echo -e "\033]50;SetProfile=Eelco\a"
		clear
	else
		cat ~/.eelco
	fi
	true
}

normal() {
	clear
	echo -e "\033]50;SetProfile=Default\a"
	clear
	true
}

source ~/.workspaces/workspace.sh
alias ws=workspace
alias wo=workspace

BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"
