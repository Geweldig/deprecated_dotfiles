call plug#begin('~/.vim/plugged')

Plug 'haya14busa/incsearch.vim'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'chriskempson/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'osyo-manga/vim-over'
Plug 'ekalinin/Dockerfile.vim'
Plug 'junegunn/goyo.vim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'

"Autocomplete
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
call plug#end()

set nocompatible

set nu
set ruler
set incsearch
filetype plugin indent on
syntax on
set backspace=2
set tabstop=4
set softtabstop=0 noexpandtab

set guicursor=

"Searching
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

set hlsearch
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)
"End searching


"Autocompletion
let g:deoplete#enable_at_startup = 1
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
"End autocompletion

"Tabs
nmap <silent> <leader>t :NERDTreeTabsToggle
let g:nerdtree_tabs_open_on_console_startup = 0
"End tabs

"Replace highlighting
let g:over_enable_auto_nohlsearch = 1
let g:over_enable_cmd_window = 1
let g:over_command_line_prompt = "> "
"End replace highlighting

"Theme
set laststatus=2
let g:airline_powerline_fonts=1
let g:airline_detect_paste=1
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='base16'

"base16
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif
"end base16
"End theme

"Markdown
let vim_markdown_preview_browser='Google Chrome'
"End markdown

"Aliases
:command OR OverCommandLine
