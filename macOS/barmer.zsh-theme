local ret_status="%(?:%{$fg[white]%}:%{$fg_bold[red]%})"

PROMPT='${ret_status}[%{$reset_color%}%{$fg_bold[blue]%}${PWD/#$HOME/~}%{$reset_color%}${ret_status}]%{$reset_color%}$(git_prompt_info)%{$reset_color%} %{$fg[blue]%}▶%{$reset_color%} '

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[red]%}!"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg_bold[green]%}?"
ZSH_THEME_GIT_PROMPT_CLEAN=""
