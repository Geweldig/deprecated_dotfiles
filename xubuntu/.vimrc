call plug#begin('~/.vim/plugged')

Plug 'haya14busa/incsearch.vim'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'ervandew/supertab'
Plug 'altercation/vim-colors-solarized'
Plug 'tomasr/molokai'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'osyo-manga/vim-over'
Plug 'shougo/vinarise.vim'
Plug 'jeffkreeftmeijer/vim-numbertoggle'

call plug#end()

set nocompatible

set nu
set ruler
set incsearch
set number relativenumber
filetype plugin indent on
syntax on
set backspace=2
set tabstop=4
set softtabstop=0 noexpandtab

"Searching
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

set hlsearch
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)
"End searching

"Tabs
nmap <silent> <leader>t :NERDTreeTabsToggle
let g:nerdtree_tabs_open_on_console_startup = 0
"End tabs

"Replace highlighting
let g:over_enable_auto_nohlsearch = 1
let g:over_enable_cmd_window = 1
let g:over_command_line_prompt = "> "
"End replace highlighting

"Aliases
:command OR OverCommandLine
