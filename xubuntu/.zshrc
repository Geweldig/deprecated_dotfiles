# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/bcrielaard/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="Chicago95"
DEFAULT_USER="bcrielaard"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="false"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git workspace)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Show motd
cat /etc/motd

weather() {
		curl "wttr.in/"$1
}
alias wttr="weather"

start_chromecast() {
		(nohup pulseaudio-dlna --codec=mp3 --bitrate=320 --encoder-backend=ffmpeg --auto-reconnect > /dev/null 2>&1 &)
}

stop_chromecast() {
		local PID=$(pidof pulseaudio-dlna)
		local ISNU="^[0-9]+$"
		if [[ $PID =~ $ISNU ]] ; then
			kill $PID
			return 0
		else
			return 1
		fi
}

update_all_zsh() {
		sudo apt update
		sudo apt upgrade
		sudo apt autoremove
		tldr --update
		upgrade_oh_my_zsh
}

doom() {
		setopt extendedglob
		declare -a local wads
		for f (~/.wads/*.(#i)wad) {
				wads+=($f)
		}
		echo "Which WAD do you want to play?"
		local COLUMNS=12
		local opt
		select opt in "${wads[@]}"
		do
				if [[ ! -z $opt ]]; then
						chocolate-doom -iwad $opt
				fi
				break
		done
}

alias :q="exit"
alias :wq="exit"
alias :x="exit"

alias emacs="vim"
alias ed="vim"
alias vi="vim"
alias nano="vim"
alias joe="vim"

alias top="htop"

alias sl="ls"
alias gti="git"

alias mn="man"
alias clc="clear"

eval $(thefuck --alias)

alias py=python3
alias open=xdg-open
alias dir="ls"

source ~/.workspaces/workspace.sh
alias ws=workspace
alias wo=workspace
alias clipboard="xclip -selection c"
